#!/bin/bash

#    This file is part of Ditty - Distributed Issue Tracking and Todos for You
#    Copyright (C) 2015 Julian Stirling
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>

#Find library directory
libdir=$(dirname "$(readlink -f "$0")")/lib/
. ${libdir}/ditty-tools

if [ "$#" -lt "1" ]; then
  err_echo "Error: ditty requires a sub-funtion to run."
  exit 1
fi

git_dir=
if [ -d .git ]; then
  git_dir=$(pwd)".git"
else
  git_dir=$(git rev-parse --git-dir 2> /dev/null)
fi
if [ ! "$git_dir" ]; then
  err_echo "Error: Not a git repository. Ditty should be run inside a git repository"
  exit 1
fi
#move to the git root (remove .git from git_dir)
cd ${git_dir::-4}

if [ -f ".ditty/ditty.yml" ]; then
  IsDitty="1"
  cd ".ditty"
fi

sub_func=$1
shift

#call subfunction
if [ "$sub_func" == "init" ]; then
  if [ "$IsDitty" ]; then
    err_echo "Error: cannot init ditty here ditty is already initalised."
    exit 1
  fi
  "${libdir}ditty-init.sh" "$@"
  exit 0
fi

if [ "$sub_func" == "new" ] || [ "$sub_func" == "list" ] || [ "$sub_func" == "edit" ] || [ "$sub_func" == "show" ] \
        || [ "$sub_func" == "close" ] || [ "$sub_func" == "category" ] || [ "$sub_func" == "mksite" ]  || [ "$sub_func" == "update-site" ]; then
  if [ ! "$IsDitty" ]; then
    err_echo "Error: ditty issues not tracked in this repository, to start tracking run ditty init."
    exit 1
  fi
  case "$sub_func" in
    new)
      "${libdir}ditty-new.sh" "$@"
      ;;
    list)
      "${libdir}ditty-list.sh" "$@"
      ;;
    edit)
      "${libdir}ditty-edit.sh" "$@"
      ;;
    show)
      "${libdir}ditty-show.sh" "$@"
      ;;
    close)
      "${libdir}ditty-close.sh" "$@"
      ;;
    category)
      "${libdir}ditty-category.sh" "$@"
      ;;
    mksite)
      "${libdir}ditty-mksite.sh" "$@"
      ;;
    update-site)
      "${libdir}ditty-update-site.sh" "$@"
      ;;
  esac
  exit 0
fi

err_echo "Error: ditty has no subfunction $sub_func"
