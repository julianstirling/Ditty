#!/bin/bash

#    This file is part of Ditty - Distributed Issue Tracking and Todos for You
#    Copyright (C) 2015 Julian Stirling
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>

libdir=$(dirname "$(readlink -f "$0")")
. ${libdir}/ditty-tools

cd ..
if [ ! -f ".ditty/dittysite.local" ]; then
  err_echo "Error: This ditty not linked to a local site. You can create a local site it with
  ditty mksite
If you have a local site the link has been removed, perhaps by a git clean operation.
you need to make a new dittysite.local file manually."
  exit 1
fi

libdir=$(dirname "$(readlink -f "$0")")
. ${libdir}/ditty-tools

site=$(read_meta "$(cat .ditty/dittysite.local)" localpath)

if [ ! -d "$site/_posts" ] || [ ! -d "$site/_data" ]; then
  err_echo "Error: $site doesn't seem to be a ditty site"
fi


#recopy all issues and config
cp ".ditty/ditty.yml" "$site/_data"
cp -a .ditty/*.md "$site/_posts"
for issue in $(ls -r ${site}/_posts/*.md 2> /dev/null); do
  if [ ! -f ".ditty/$(basename $issue)" ]; then
    echo "Issue $(basename $issue) deleted"
    rm $issue
  fi
done
