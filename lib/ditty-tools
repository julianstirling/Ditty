#    This file is part of Ditty - Distributed Issue Tracking and Todos for You
#    Copyright (C) 2015 Julian Stirling
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>

#Functions to be included into other scripts

function err_echo(){
  echo -e "\e[1m\e[91m$1\e[0m" 1>&2
}

function last_meta_line(){
  local lnum="$(grep "^\-\-\-" -n $1 |cut -f1 -d:|head -n 2)" #output the first two lines where --- appears (numbers on new lines)
  echo $(($(echo "$lnum"|sed -n '2p')+1)) #return line after second ---
}

function check_meta(){
  local tmp
  echo $(echo "$1"|grep "^[[:space:]]*$2:"|wc -l) #grep for first input for second followed by : , outout number of lines matched
}

function read_meta(){
  local tmp
  tmp=$(echo "$1"|grep "^[[:space:]]*$2:"|cut -f2 -d:) #grep for first input for second followed by : then cut after :
  echo ${tmp%%#*} # remove any comments after # using paramter expansion
}

function meta_replace(){
  local n
  n=$(grep "^[[:space:]]*$2:[[:space:]]*$3" -n $1|cut -f1 -d:|head -n 1) #get first matching line
  if [ "$n" ]; then #line must exist
    if [ "${1: -3}" == ".md"  ] &&  [ "$n" -gt "$(last_meta_line $1)" ]; then
      exit 0 #cannot replace from markdown file after last line of meta
    fi  
    sed -i "${n}s/.*/$2: $4/" "$1"
  fi
}

function get_meta(){
  local lnums="$(grep "^\-\-\-" -n $1 |cut -f1 -d:|head -n 2)" #output the first two lines where --- appears (numbers on new lines)
  echo "$(sed -n $(($(echo "$lnums"|sed -n '1p')+1)),$(($(echo "$lnums"|sed -n '2p')-1))p $1)" #cut out section between ---
}


function get_contents(){
  echo "$(sed -n $(last_meta_line $1),\$p $1)" #return after line
}

function remove_white(){ #remove surrounding whitespace
  echo "$(echo $1 | sed -e 's/^[[:space:]]*//' | sed -e 's/[[:space:]]*$//')"
}

function check_if_issue(){
  local OPTIND opt
  local quiet=
  while getopts ":q" opt; do
    case $opt in
      q)
        quiet='1'
      ;;
    esac
  done

  shift "$((OPTIND - 1))"
  if [ ! -f "$1" ]; then
    err_echo "Error: file $1 doesn't seem to exist."
    exit 1
  fi
  local lnums="$(grep "^\-\-\-" -n $1 |cut -f1 -d:)" #line numbers of lines that start ---
  local nlines=$(echo "$lnums"|wc -l) #number of lines
  if [ "$nlines" -ge "2" ]; then #check there are 2 or more
    lnums=$(echo "$lnums"|head -n 2)
  else
    if [ ! "$quiet" ]; then
      err_echo "Error: file doesn't seem to be a ditty issue."
    fi
    exit 1
  fi
  local meta="$(get_meta $1)"
  if [ $(read_meta "$meta" categories) != 'Ditty' ]; then
    if [ ! "$quiet" ]; then
      err_echo "Error: This jeykll post is not a ditty issue."
    fi
    exit 1
  fi
  echo "1"
}

function issue_matches(){
  #match status, if match fails exit
  local status
  case $2 in
    0)
      if [ "$(read_meta "$meta" ditty_status)" != "open" ]; then exit 0; fi
      ;;
    1)
      if [ "$(read_meta "$meta" ditty_status)" != "closed" ]; then exit 0; fi
      ;;
    2)
      ;;
    *)
      err_echo "Error: unknown status sent to match"
      exit 1
      ;;
  esac
  echo 1 #if didn't already exit issue matches
}