#!/bin/bash

#    This file is part of Ditty - Distributed Issue Tracking and Todos for You
#    Copyright (C) 2015 Julian Stirling
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>

libdir=$(dirname "$(readlink -f "$0")")
. ${libdir}/ditty-tools

file="$(date +"%Y-%m-%d-%H%M%S")_$(git config user.name).md"
if [ ! -f ditty.yml ]
then exit 1
fi

echo "--- 
##Don't edit this section
layout: ditty_issue
title:  \"$1\"
date:   $(date +"%Y-%m-%d %H:%M:%S")
ditty_status: open
categories: Ditty
ditty_cat: None
ditty_milestone: None
##Edit after the \"---\"
---" > $file

nano $file
