#!/bin/bash

#    This file is part of Ditty - Distributed Issue Tracking and Todos for You
#    Copyright (C) 2015 Julian Stirling
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>

libdir=$(dirname "$(readlink -f "$0")")
. ${libdir}/ditty-tools

if [ -f "dittysite.local" ]; then
  err_echo "Error: This ditty is already linked to a local site. To unlink it delete dittysite.local in the .ditty directory"
  exit 1
fi

# move to git root
cd ..

if [ ! -d "$1" ]; then
  err_echo "Error: Cannot make ditty site in $1, it is not a directory."
  exit 1
fi

if [ "$(ls -A $1)" ]; then
  err_echo "Error: Cannot make ditty site in a folder that is not empty."
  exit 1
fi
 
cp -a "$(dirname "$(readlink -f "$0")")/../site_template/." $1

cp ".ditty/ditty.yml" "$1/_data"
cp -a .ditty/*.md "$1/_posts"
echo "localpath: $1"> .ditty/dittysite.local

