#! /bin/bash

#    This file is part of Ditty - Distributed Issue Tracking and Todos for You
#    Copyright (C) 2015 Julian Stirling
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>


libdir=$(dirname "$(readlink -f "$0")")
. ${libdir}/ditty-tools

issue=$(basename "$1")

if [ "$#" -ne "1" ]; then
  err_echo "Error: expecting exactly one argument for show"
  exit 1
fi
if [ ! "$(check_if_issue "$issue")" ]; then
  err_echo "Error: input file is not ditty issue"
  exit 1
fi

meta=$(get_meta $1)
title=$(read_meta "$meta" title)
status=$(read_meta "$meta" ditty_status)
date=$(read_meta "$meta" date)
cat=$(read_meta "$meta" ditty_cat)
mile=$(read_meta "$meta" ditty_milestone)
cont=$(get_contents $1)
echo -e "\e[1m\e[35m
Title         -  ${title:1:-1}
Status        -  $status
Date created  -  $date
Category      -  $cat
Milestone     -  $mile
\e[0m"
echo "$cont"
