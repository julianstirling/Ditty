#! /bin/bash

#    This file is part of Ditty - Distributed Issue Tracking and Todos for You
#    Copyright (C) 2015 Julian Stirling
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>


libdir=$(dirname "$(readlink -f "$0")")
. ${libdir}/ditty-tools

closed=
all=

while getopts ":ca" opt; do
  case $opt in
    c)
      closed='1'
      ;;
    a)
      all='1'
      ;;
    \?)
      err_echo "Invalid option: -$OPTARG"
      exit 1
      ;;
  esac
done

shift "$((OPTIND - 1))"

if [ $((closed+all)) -gt "1" ]; then
  err_echo "Error: expecting only one of the closed or all options"
  exit 1
fi

printed_head=
for issue in $(ls -r *.md 2> /dev/null); do
  if [ "$(check_if_issue -q "$issue")" ]; then
    meta=$(get_meta $issue)
    if [ "$(issue_matches "$meta" "$((closed+2*all))")" ]; then
      if [ ! "$printed_head" ]; then
        printf "\n\n%-40s\t%-40s" "File" "Issue title"
        if [ "$all" ]; then
          printf "\tStatus"
        fi
        printf "\n"
        for (( i=1; i<=$(tput cols); i++ ));do printf "-"; done
        printf "\n"
        printed_head=1
      fi
      title=$(read_meta "$meta" title)
      title=$(if [ "$(echo "$title"|wc -c)" -gt "39" ]; then echo "${title:1:36}..."; else echo "${title:1:-1}"; fi)
      issue=$(if [ "$(echo "$issue"|wc -c)" -gt "39" ]; then echo "${issue:0:36}..."; else echo "$issue"; fi)
      printf "%-40s\t%-40s" "$issue" "$title"
      if [ "$all" ]; then
        printf "\t$(read_meta "$meta" ditty_status)"
      fi
      printf "\n"
    fi
  fi
done
printf "\n"
cd ".."
exit 0
