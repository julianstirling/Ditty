#! /bin/bash

#    This file is part of Ditty - Distributed Issue Tracking and Todos for You
#    Copyright (C) 2015 Julian Stirling
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>

libdir=$(dirname "$(readlink -f "$0")")
. ${libdir}/ditty-tools

quiet=
remove=
if [ ! "$1" ]; then
  mode="list"
else
  case $1 in
    add)
      mode="add"
      ;;
    remove)
      mode="rename"
      remove=true #remove is a sepecial case of rename
      ;;
    rename)
      mode="rename"
      ;;
    -q) #no need to use getopts -q is only option and only available for list
      mode="list"
      quiet=true
      ;;
    *)
      err_echo "Error: Mode $1 is not understood for ditty category"
      exit 1
  esac
fi

shift

if [ "$mode" == "add" ] || [ "$mode" == "rename" ]; then
  if [ "$#" -lt "1" ]; then
    err_echo "Error: Categories to modify not specified."
    exit 1
  elif [ "$mode" == "rename" ] && [ ! "$remove" ] && [ "$(($#%2))" == "1" ]; then
    err_echo "Error: When renaming categories must be entered in pairs"
    exit 1
  fi
fi

meta=$(cat ditty.yml)
cat_str=
case $(check_meta "$meta" categories) in
  0)
    cats=""
    ;;
  1)
    cat_str=$(read_meta "$meta" categories)
    #if [ $(
    cats=$(echo $cat_str|grep '^[[:space:]]*\[[a-zA-Z0-9_,]*\][[:space:]]*$')
    if [ "$cats" ]; then
      cats="$(echo $cats| sed -e 's/\[//'| sed -e 's/\]//')"
    else
      err_echo "Error: Categories incorrectly formatted"
      exit 1
    fi
    ;;
  *)
    err_echo "Error: More than one set of categories defined"
    exit 1
esac

case $mode in
  list)
    n='-1'
    if [ ! "$quiet" ]; then
      echo "Available categories are:"
    fi
    cats="None,$cats"
    for cat in $(echo $cats | tr , '\n'); do
      n=$(($n+1))     
      if [ "$quiet" ]; then
        echo "${cat}"
      else
        echo "#$n : ${cat}"
      fi
    done
    ;;
  add)
    while [ "$1" ];do
      if [ "$1" == "None" ]; then
        err_echo "Error: Category None is explicit and can't be added"
      elif [ "$(echo $cats|grep -w "$1")" ];then
        err_echo "Error: Category $1 already exits and can't be added again"
      else
        cats="$cats,$1"
      fi
      shift
    done
    if [ "$cat_str" ];then #if line already in file
      cat_str=$(echo $cat_str|sed -e 's/\[/\\[/'|sed -e 's/\]/\\]/')
      meta_replace ditty.yml categories "$cat_str" "[$cats]" #escape sqare brackets in sat{_str for grep in sub function
    else
      echo "categories: [$cats]" >> ditty.yml
    fi
    ;;
  rename)
    if [ ! "$cat_str" ];then
      err_echo "Error: Ditty has no categories to remove or rename"
      exit 1
    fi
    while [ "$1" ];do
      if [ "$1" == "None" ]; then
        err_echo "Error: Category None is explicit and can't be removed"
      elif [ "$(echo $cats|grep -w "$1")" ];then
        if [ "$remove" ] || [ "$2" == "None" ]; then #rename to nothing or to argument 2
          cats=$(echo $cats | sed -e "s/\b$1\b//")
        else
          if [ "$(echo $cats|grep -w "$2")" ];then
            cats=$(echo $cats | sed -e "s/\b$1\b//") # dont create a second if already exists
          else
            cats=$(echo $cats | sed -e "s/\b$1\b/$2/")
          fi
        fi
      else
        err_echo "Error: Category $1 cannot be removed as it doens't exits"
      fi
    shift
    if [ ! "$remove" ]; then
      shift #rename works in pairs, need to shift twice
    fi
    done
    cats=$(echo $cats | sed -e "s/,\+/,/g"| sed -e "s/^,/,/" | sed -e "s/,$//") # remove duplicate, leading, or traling commas
    cat_str=$(echo $cat_str|sed -e 's/\[/\\[/'|sed -e 's/\]/\\]/') #escape sqare brackets in sat{_str for grep in sub function
    meta_replace ditty.yml categories "$cat_str" "[$cats]"
    ;;
esac